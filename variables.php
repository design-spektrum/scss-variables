<?php
/**
 * Script for managing SCSS Variables
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright Patryk Adamczyk © 2018
 * @version 0.9beta1
 */
define("PARAMETER_CMS_VERSION", "0.9.beta1");
/**
 * <Region>
 * <title>BEZ SERWERA</title>
 */
if(in_array($_SERVER["SERVER_NAME"], ["serwer1523088.home.pl"]))
{
    header("Status: 403 Forbidden", true, 403);
    http_response_code(403);
    header($_SERVER["SERVER_PROTOCOL"]." 403 Forbidden");
    exit();
}
/**
 * </Region>
 */
/**
 * <Region>
 * <title>DEBUG</title>
 */
require_once "deps/PW_Debug.php";
/**
 * </Region>
 */
/**
 * Localization Array
 */
$lang = array();
/**
 * Warnings Array
 */
$warn = array();
/**
 * Localization of Modules
 */
$lang_mod = array();
/**
 * Modules that are enabled
 */
$modules_enabled = array();
/**
 * States Variable
 */
$states = array();
/**
 * Array of all Themes
 */
$themes = array();
/**
 * Current Theme
 */
$theme = "";
/**
 * Theme Localization
 */
$lang_theme = array();
/**
 * Options Array
 */
$options = array();
/**
 * Localization of Sections
 */
$lang_sections = array();
/**
 * Tooltips for sections
 */
$tooltips_sections = array();
/**
 * This class handles all base configuration of App.
 */
class SuperApp
{
    /**
     * Loads every what is needed for site to work
     */
    static function LoadSite()
    {
        global $lang, $warn, $lang_mod, $modules_enabled, $themes, $theme, $lang_theme, $options, $lang_sections, $tooltips_sections;
        $lang = SuperApp::LoadLanguages();
        $warn = SuperApp::LoadWarns();
        $lang_mod = SuperApp::LoadModuleLanguages();
        $modules_enabled = SuperApp::LoadModulesEnabled();
        $themes = SuperApp::LoadThemes();
        SuperApp::saveCurrentTheme();
        $theme = SuperApp::LoadCurrentTheme();
        $lang_theme = SuperApp::LoadThemeLanguages();
        $options = SuperApp::LoadOptions();
        $lang_sections = SuperApp::LoadSectionLanguages();
        $tooltips_sections = SuperApp::LoadSectionTooltips();
    }
    /**
     * Load Localization from settings file
     */
    static function LoadLanguages()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["lang"];
    }
    /**
     * Load Warnings from settings file
     */
    static function LoadWarns()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["warn"];
    }
    /**
     * Load Localization for modules from settings file
     */
    static function LoadModuleLanguages()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["lang-modules"];
    }
    /**
     * Load which modules are enabled from configuration file
     */
    static function LoadVModulesEnabled()
    {
        return json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        )["enabled"];
    }
    /**
     * Load which modules are enabled
     */
    static function LoadModulesEnabled()
    {
        return array_merge(["base"], SuperApp::LoadVModulesEnabled());
    }
    /**
     * Load all modules from config
     */
    static function LoadVAllModules()
    {
        return json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        )["all"];
    }
    /**
     * Load default modules from config
     */
    static function LoadDefaultModules()
    {
        return json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        )["default"];
    }
    /**
     * Load all modules
     */
    static function LoadAllModules()
    {
        $ret = array();
        $r1 = SuperApp::LoadVAllModules();
        $r2 = SuperApp::LoadDefaultModules();
        foreach($r1 as $r)
        {
            if(in_array($r, $r2))
            {
                continue;
            }
            $ret[] = $r;
        }
        return $ret;
    }
    /**
     * Load list of themes
     */
    static function LoadThemes()
    {
        $r = json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."themes.json"
            ),
            true
        );
        return array_merge(["default"], $r["themes"]);
    }
    /**
     * Load current theme
     */
    static function LoadCurrentTheme()
    {
        $r = json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        );
        return (is_array($r) ? (isset($r["theme"]) ? $r["theme"] : "default") : "default");
    }
    /**
     * Load Localization for themes from settings file
     */
    static function LoadThemeLanguages()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["lang-themes"];
    }
    /**
     * Load Localization for sections from settings file
     */
    static function LoadSectionLanguages()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["lang-sections"];
    }
    /**
     * Load tooltips for sections from settings file
     */
    static function LoadSectionTooltips()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["tooltips-sections"];
    }
    /**
     * Save current theme
     */
    static function saveCurrentTheme()
    {
        $r = json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        );
        $r["theme"] = (isset($_POST["theme-variables"]) ? $_POST["theme-variables"] : $r["theme"]);
        return file_put_contents(
            __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json",
            json_encode($r, JSON_PRETTY_PRINT)
        );
    }
    /**
     * Load options from settings file
     */
    static function LoadOptions()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["options"];
    }
    /**
     * Filter results to be unique
     */
    static function uniqueArray($t)
    {
        $ret = array();
        foreach($t as $v)
        {
            if(!in_array($v, $ret))
            {
                $ret[] = $v;
            }
        }
        return $ret;
    }
    /**
     * Add to list of themes
     */
    static function AddTheme($t)
    {
        global $themes;
        $r = json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."themes.json"
            ),
            true
        );
        $r["themes"] = SuperApp::uniqueArray(array_merge([$t],$r["themes"]));
        $f = json_encode($r, JSON_PRETTY_PRINT);
        $s = file_put_contents(
            __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."themes.json",
            $f
        );
        $themes = SuperApp::LoadThemes();
        return $s;
    }
    /**
     * Load Animation Names from config
     */
    static function LoadAnimationNames()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."params_generator.settings.json"
                ),
                true
            )["animation-names"];
    }
    /**
     * Load localization of animation names
     */
    static function LoadAnimationNamesLanguages()
    {
        return json_decode(
                file_get_contents(
                    __DIR__.DIRECTORY_SEPARATOR."variables.settings.json"
                ),
                true
            )["lang-animation-names"];
    }
}
// Load Site configuration and other things.
SuperApp::LoadSite();
/**
 * This class have tools needed for things in other classes.
 */
class Tools
{
    /**
     * Preg Match All result to array
     * @param array $result preg_match result
     */
    static function pregMatchResult2Array($result)
    {
        $return = [];
        foreach (range(0, count($result[0])-1) as $k)
        {
            $return[$k]["varname"] = $result[1][$k];
            $return[$k]["value"] = $result[2][$k];
        }
        return $return;
    }
    /**
     * Get variable with specified name
     */
    static function getOne($key, $array)
    {
        foreach($array as $v)
        {
            if($v["varname"] == $key)
            {
                return $v;
            }
        }
        return NULL;
    }
    /**
     * Checking is this option should be default.
     */
    static function checkToDefault($v, $r)
    {
        if($v == $r)
        {
            echo("selected");
        }
    }
    /**
     * Checking is this option should be default on checkbox
     */
    static function checkToDefaultCheckbox($v, $r)
    {
        if(in_array($v,$r))
        {
            echo("checked");
        }
    }
    /**
     * Preg match result converter to array of section
     */
    static function pregMatchResult2SectionArray($result)
    {
        $return = [];
        foreach (range(0, count($result[0])-1) as $k)
        {
            $return[$result[2][$k]] = $result[1][$k];
        }
        return $return;
    }
    /**
     * Changing warning to CSS class
     */
    static function warnToClass($t1)
    {
        global $warn;
        $t = (isset($warn[$t1]) ? $warn[$t1] : NULL);
        switch($t)
        {
            case "warn":
                echo(' list-group-item-action list-group-item-warning');
                break;
            case "warning":
                echo(' list-group-item-action list-group-item-warning');
                break;
            case "danger":
                echo(' list-group-item-action list-group-item-danger');
                break;
            case "success":
                echo(' list-group-item-action list-group-item-success');
                break;
            case "info":
                echo(' list-group-item-action list-group-item-info');
                break;
            case "primary":
                echo(' list-group-item-action list-group-item-primary');
                break;
        }
    }
    /**
     * Get Path to module
     * @param string $mod Module
     * @param string $ext Extension
     * @return string Path to Module
     */
    static function modToPath($mod, $ext="scss")
    {
        global $theme;
        return __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR.$theme.DIRECTORY_SEPARATOR.$mod.".".$ext;
    }
    /**
     * Get Path to theme
     * @param string $theme Theme
     * @return string Path to Theme
     */
    static function themeToPath($theme)
    {
        return __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR.$theme;
    }
    /**
     * Check is option enabled
     * @param string $optionname Name of Option to Check
     * @return bool Option Status
     */
    static function getOptionStatus($optionname)
    {
        global $options;
        if(isset($options[$optionname]) && $options[$optionname] == true)
        {
            return True;
        }
        return False;
    }
    /**
     * Trim Overload Function
     * @param string $str String to trim
     * @param int $end How many times it should trim that string
     * @return string Trimmed String
     */
    static function trimOV($str, $end=10)
    {
        $ret = $str;
        $x = range(0, $end);
        foreach($x as $v)
        {
            $ret = trim($ret);
        }
        return $ret;
    }
    /**
     * Get Tooltip for section
     * @param string $section Section Name
     * @return string Tooltip
     */
    static function tooltip($section)
    {
        global $tooltips_sections;
        if(isset($tooltips_sections[$section]))
        {
            return 'data-toggle="tooltip" data-placement="top" title="'.$tooltips_sections[$section].'"';
        }
        return '';
    }
}
/**
 * Parser
 */
class PW_SCSS__Parser
{
    /**
     * This function parses provided file to tokens table
     * 
     * This function does exactly this steps:
     * 1. Loads file to parse
     * 2. Parses variables and sections
     * 3. Organizes results of parsing
     * 4. Tokenizes all parsing results
     * 
     * Tokenizer Result is in that format
     * [
     *      [
     *          "type" => "Here is type of token (section or variable)",
     *          "value" => "Value of token"
     *      ]
     * ]
     * @param string $file File to parse
     * @return array Tokens from parser
     */
    static function parse($file)
    {
        // Get File Content to Parse
        $file_text = file_get_contents($file);
        // Temporary Variable
        $tmp = array();
        // Parse Variables
        $t = preg_match_all(
            '/\$(?P<varname>[\S]+):[\s]*(?P<value>[ \S]+);/',
            $file_text,
            $tmp["variables"]
        );
        // Parse Sections "h1"
        $t2 = preg_match_all(
            '/\/\*(?P<sectionname>[^\*]+)\*\/[\h\v]*\$(?P<varname>[^\:]+):/',
            $file_text,
            $tmp["h1"]
        );
        // Parse Sections "h2"
        $t3 = preg_match_all(
            '/\/\*\*(?P<sectionname>[^\*]+)\*\*\/[\h\v]*\$(?P<varname>[^\:]+):/',
            $file_text,
            $tmp["h2"]
        );
        // Parse Sections "h3"
        $t4 = preg_match_all(
            '/\/\*\*\*(?P<sectionname>[^\*]+)\*\*\*\/[\h\v]*\$(?P<varname>[^\:]+):/',
            $file_text,
            $tmp["h3"]
        );
        ### Variables
        // Temporary Variable 2
        $return = array();
        // Change how arranged is array of parsed data
        foreach (range(0, count($tmp["variables"][0])-1) as $k)
        {
            $return["variables"][$k]["varname"] = $tmp["variables"]["varname"][$k];
            $return["variables"][$k]["value"] = $tmp["variables"]["value"][$k];
        }
        foreach (range(0, count($tmp["h1"][0])-1) as $k)
        {
            if($k < 0 || count($tmp["h1"][0]) == 0) { break; }
            $return["section"][$tmp["h1"][2][$k]] = $tmp["h1"][1][$k];
        }
        foreach (range(0, count($tmp["h2"][0])-1) as $k)
        {
            if($k < 0 || count($tmp["h2"][0]) == 0) { break; }
            $return["section-h2"][$tmp["h2"][2][$k]] = $tmp["h2"][1][$k];
        }
        foreach (range(0, count($tmp["h3"][0])-1) as $k)
        {
            if($k < 0 || count($tmp["h3"][0]) == 0) { break; }
            $return["section-h3"][$tmp["h3"][2][$k]] = $tmp["h3"][1][$k];
        }
        #var_dump($return);
        ### Tokenizer from results
        // Tokens Variable
        $tokens = array();
        // Max Iterations for loop
        $max_iterations = count($return["variables"]);
        // Iterator
        $i = 0;
        // Token Creator Loop
        foreach($return["section"] as $k => $v)
        {
            while (True)
            {
                // Break on max_iterations
                if($i >= $max_iterations)
                {
                    break;
                }
                // Section H1 Found
                if($k == $return["variables"][$i]["varname"])
                {
                    $tokens[] = [
                        "type" => "section",
                        "value" => $v
                    ];
                    break;
                }
                // Section H2 Found
                if(isset($return["section-h2"][$return["variables"][$i]["varname"]]))
                {
                    $tokens[] = [
                        "type" => "section2",
                        "value" => $return["section-h2"][$return["variables"][$i]["varname"]]
                    ];
                }
                // Section H3 Found
                if(isset($return["section-h3"][$return["variables"][$i]["varname"]]))
                {
                    $tokens[] = [
                        "type" => "section3",
                        "value" => $return["section-h3"][$return["variables"][$i]["varname"]]
                    ];
                }
                // Variable Found
                $tokens[] = [
                    "type" => "variable",
                    "value" => $return["variables"][$i]
                ];
                // Increment Iterator
                $i++;
            }
            // Break on max_iterarations
            if($i >= $max_iterations)
            {
                break;
            }
            // Variable Found
            $tokens[] = [
                "type" => "variable",
                "value" => $return["variables"][$i]
            ];
            // Increment Iterator
            $i++;
        }
        // Variables in last section
        while(True)
        {
            // Break on max_iterations
            if($i >= $max_iterations)
            {
                break;
            }
            // Section H2 Found
            if(isset($return["section-h2"][$return["variables"][$i]["varname"]]))
            {
                $tokens[] = [
                    "type" => "section2",
                    "value" => $return["section-h2"][$return["variables"][$i]["varname"]]
                ];
            }
            // Section H3 Found
            if(isset($return["section-h3"][$return["variables"][$i]["varname"]]))
            {
                $tokens[] = [
                    "type" => "section3",
                    "value" => $return["section-h3"][$return["variables"][$i]["varname"]]
                ];
            }
            // Variable Found
            $tokens[] = [
                "type" => "variable",
                "value" => $return["variables"][$i]
            ];
            // Iterator Increment
            $i++;
        }
        //var_dump($tokens); exit();
        // Return Tokens
        return $tokens;
    }
}
/**
 * Main App Class
 */
class App
{
    /**
     * Function called to show some alerts with it is needed
     */
    static function getSpecialAnnouncement()
    {/*
        ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width: 100%; text-align: center;">
            <h4 class="alert-heading">BETA</h4>
            <p>
                Uwaga ta strona jest w wersji BETA. Korzystarz na własną odpowiedzialność.
            </p>
            <hr>
            <p class="mb-0">
                Ta wersja może nie działać perfekcyjnie!
            </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php*/
    }
    /**
     * Save Announcement
     * @todo This function is not used anymore for now by AJAX.
     */
    static function getSaveAnnouncement()
    {
        global $states;
        if(isset($states["saved"]))
        {
            ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 100%; text-align: center;">
                <h4 class="alert-heading">Zapisano!</h4>
                <p>
                    Zapisano wszystkie pliki oraz wygenerowano plik <code>variable.scss</code>
                </p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php
        }
    }
    /**
     * Main function to process requests
     */
    static function processRequest()
    {
        global $theme;
        if(isset($_POST["form__updater_of_themes"]))
        {
            if($theme == "default")
            {
                print("REALLY?!");
                header("Location: variables.php");
            }
            $path = Tools::themeToPath("default").DIRECTORY_SEPARATOR."*.scss";
            //var_dump($path);
            $files_default = array();
            foreach(glob($path) as $file)
            {
                $files_default[] = str_replace(Tools::themeToPath("default").DIRECTORY_SEPARATOR, "", $file);
            }
            $path = Tools::themeToPath($theme).DIRECTORY_SEPARATOR."*.scss";
            $files_theme = array();
            foreach(glob($path) as $file)
            {
                $files_theme[] = str_replace(Tools::themeToPath($theme).DIRECTORY_SEPARATOR, "", $file);
            }
            $files_diff = array();
            foreach($files_default as $f)
            {
                if(!in_array($f, $files_theme))
                {
                    $files_diff[] = $f;
                }
            }
            foreach($files_diff as $f)
            {
                $src = Tools::themeToPath("default").DIRECTORY_SEPARATOR.$f;
                $dest = Tools::themeToPath($theme).DIRECTORY_SEPARATOR.$f;
                if(!copy($src, $dest))
                {
                    ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 100%; text-align: center;">
                        <h4 class="alert-heading">Błąd!</h4>
                        <p>
                            Źle zaktualizowane pliki!
                        </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                    break;
                }
            }
            // die("42");
            // exit();
            //App::moduleManagerRequest();
            header("Location: variables.php");
        }
        if(isset($_POST["form__module_manager"]))
        {
            App::moduleManagerRequest();
            header("Location: variables.php");
        }
        if(isset($_POST["form__icon__submit__button"]))
        {
            App::iconsProcessRequest();
            header("Location: sass.php?redirect_after=variables.php&theme=$theme");
        }
        if(isset($_POST["theme-variables"]))
        {
            App::variablesSCSS_Creator();
            header("Location: sass.php?redirect_after=variables.php&theme=$theme");
        }
        if(isset($_POST["theme-add-submit"]))
        {
            #Creating Theme
            $t = $_POST["theme-name"];
            if(!is_dir(Tools::themeToPath($t)))
            {
                mkdir(Tools::themeToPath($t));
                $path = Tools::themeToPath("default").DIRECTORY_SEPARATOR."*.scss";
                //var_dump($path);
                foreach(glob($path) as $file)
                {
                    $src = $file;
                    $dest = str_replace(Tools::themeToPath("default"), Tools::themeToPath($t), $file);
                    if(!copy($src, $dest))
                    {
                        ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 100%; text-align: center;">
                            <h4 class="alert-heading">Błąd!</h4>
                            <p>
                                Źle utworzony motyw!
                            </p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                        break;
                    }
                }
                SuperApp::AddTheme($t);
            }
            else
            {
                ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width: 100%; text-align: center;">
                    <h4 class="alert-heading">Błąd!</h4>
                    <p>
                        Motyw istnieje!
                    </p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
            }
        }
        //var_dump($_REQUEST);
        if(isset($_REQUEST["dscms_varform_submit"]))
        {
            App::saveChangesToJSON();
            App::saveFilesWithNewData();
            App::variablesSCSS_Creator();
            header("Location: sass.php?redirect_after=variables.php&theme=$theme");
        }
        if(isset($_GET["API"]))
        {
            App::apiRenderer();
            exit();
        }
    }
    /**
     * Save Changes to JSON
     * This is for logging like Transactions.json.
     */
    static function saveChangesToJSON()
    {
        if(isset($options["transaction-logger-save"]) && $options["transaction-logger-save"] == true)
        {
            $path = __DIR__.DIRECTORY_SEPARATOR."variables-transactions.json";
            $f = file_get_contents($path);
            $json = json_decode($f, true);
            $json[date("dmYHiu-U")] = array_merge(["__type__" => "SAVE--NEW-DATA"],$_REQUEST);
            file_put_contents($path, json_encode($json, JSON_PRETTY_PRINT));
        }
        if(function_exists("pwubugtrack_send"))
        {
            $json = [];
            $json[date("dmYHiu-U")] = array_merge(["__type__" => "SAVE--NEW-DATA"],$_REQUEST);
            $data = json_encode($json);
            pwubugtrack_send("LOGGER_SAVE", $data);
        }
    }
    /**
     * Save Tokens to JSON
     * This is for logging like Transaction.json.
     */
    static function saveTokensToJSON($tokens)
    {
        if(isset($options["transaction-logger-save"]) && $options["transaction-logger-parse"] == true)
        {
            $path = __DIR__.DIRECTORY_SEPARATOR."variables-transactions.json";
            $f = file_get_contents($path);
            $json = json_decode($f, true);
            $json[date("dmYHiu-U")] = array_merge(["__type__" => "PARSED--TOKENS"],$tokens);
            file_put_contents($path, json_encode($json, JSON_PRETTY_PRINT));
        }
        if(function_exists("pwubugtrack_send"))
        {
            $json = [];
            $json[date("dmYHiu-U")] = array_merge(["__type__" => "PARSED--TOKENS"],$tokens);
            $data = json_encode($json);
            pwubugtrack_send("LOGGER_PARSER", $data);
        }
    }
    /**
     * Save all files with new data
     */
    static function saveFilesWithNewData()
    {
        global $modules_enabled, $states;
        foreach($modules_enabled as $mod)
        {
            $path = Tools::modToPath($mod);
            App::saveFileWithNewData($path);
        }
        $states["saved"] = True;
    }
    /**
     * Save file with new data
     */
    static function saveFileWithNewData($path)
    {
        $matches = [];
        $file = file_get_contents($path);
        $t = preg_match_all('/\$(?P<varname>[\S]+):[\s]*(?P<value>[^;]+);/', $file, $matches);
        $t = Tools::pregMatchResult2Array($matches);
        $result = [];
        foreach($_REQUEST as $k => $v)
        {
            if(in_array($k, $matches[1]))
            {
                $result[$k] = $v;
            }
        }
        //var_dump($result);
        $f = $file;
        foreach($result as $k => $v)
        {
            $v = (($v != "") ? $v : "false");
            $ttt = Tools::getOne($k, $t)["value"];
            // No for Regex Injection
            $ttt = str_replace("(", "\\(", $ttt);
            $ttt = str_replace(")", "\\)", $ttt);
            $ttt = str_replace(".", "\\.", $ttt);
            $replacing = '/\$'.Tools::getOne($k, $t)["varname"].':[\s]*'.$ttt.';/';
            $replace = '$'.$k.': '.$v.';';
            $f = preg_replace($replacing, $replace, $f);
        }
        $test = file_put_contents($path, $f);
    }
    /**
     * Create Variables.SCSS
     */
    static function variablesSCSS_Creator()
    {
        global $modules_enabled, $theme;
        # Data Creator
        $data = '/*** Ten plik został wygenerowany przez Panel Zarządzania Zmiennymi ***/';
        $data .= "\n";
        $data .= '/* Nie zmieniaj tego pliku ręcznie. */';
        $data .= "\n";
        foreach($modules_enabled as $mod)
        {
            //@import 'variables/banners.scss';
            $data .= '@import "variables/'.$theme.'/'.$mod.'.scss";';
            $data .= "\n";
        }
        # Saving to variables.scss
        $path = __DIR__.DIRECTORY_SEPARATOR.'scss-lib'.DIRECTORY_SEPARATOR.'variables.scss';
        $test = file_put_contents($path, $data);
    }
    /**
     * Input Renderer
     */
    static function inputRenderer($value)
    {
        global $lang, $warn;
        ?>
        <li class="list-group-item<?php Tools::warnToClass($value["varname"]); ?>">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" title="<?php echo($value["varname"]); ?>" for="<?php echo($value["varname"]); ?>">
                    <?php echo((isset($lang[$value["varname"]]) ? $lang[$value["varname"]] : $value["varname"])); ?>
                </label>
            </div>
            <?php if(preg_match("/(\S)+-transform/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="none" title="none" <?php Tools::checkToDefault($value["value"], "none"); ?>>Brak Transformacji</option>
                <option value="capitalize" title="capitalize" <?php Tools::checkToDefault($value["value"], "capitalize"); ?>>Pierwsze litery słów dużą literą</option>
                <option value="uppercase" title="uppercase" <?php Tools::checkToDefault($value["value"], "uppercase"); ?>>Wszystkie litery dużą literą</option>
                <option value="lowercase" title="lowercase" <?php Tools::checkToDefault($value["value"], "lowercase"); ?>>Wszystkie litery małą literą</option>
                <option value="initial" title="initial" <?php Tools::checkToDefault($value["value"], "initial"); ?>>Domyślna Wartość Tranformacji</option>
                <option value="inherit" title="inherit" <?php Tools::checkToDefault($value["value"], "inherit"); ?>>Dziedzicz po rodzicu</option>
            </select>
            <?php elseif(preg_match("/(\S)+-weight/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="normal" title="normal" <?php Tools::checkToDefault($value["value"], "normal"); ?>>Normalna Czcionka</option>
                <option value="bold" title="bold" <?php Tools::checkToDefault($value["value"], "bold"); ?>>Pogrubiona Czcionka</option>
                <option value="bolder" title="bolder" <?php Tools::checkToDefault($value["value"], "bolder"); ?>>Bardziej pogrubiona czcionka</option>
                <option value="lighter" title="lighter" <?php Tools::checkToDefault($value["value"], "lighter"); ?>>Cieńka czcionka</option>
                <option value="100" title="100" <?php Tools::checkToDefault($value["value"], "100"); ?>>100</option>
                <option value="200" title="200" <?php Tools::checkToDefault($value["value"], "200"); ?>>200</option>
                <option value="300" title="300" <?php Tools::checkToDefault($value["value"], "300"); ?>>300</option>
                <option value="400" title="400" <?php Tools::checkToDefault($value["value"], "400"); ?>>400</option>
                <option value="500" title="500" <?php Tools::checkToDefault($value["value"], "500"); ?>>500</option>
                <option value="600" title="600" <?php Tools::checkToDefault($value["value"], "600"); ?>>600</option>
                <option value="700" title="700" <?php Tools::checkToDefault($value["value"], "700"); ?>>700</option>
                <option value="800" title="800" <?php Tools::checkToDefault($value["value"], "800"); ?>>800</option>
                <option value="900" title="900" <?php Tools::checkToDefault($value["value"], "900"); ?>>900</option>
                <option value="initial" title="initial" <?php Tools::checkToDefault($value["value"], "initial"); ?>>Domyślna Wartość Tranformacji</option>
                <option value="inherit" title="inherit" <?php Tools::checkToDefault($value["value"], "inherit"); ?>>Dziedzicz po rodzicu</option>
            </select>
            <?php elseif(preg_match("/(\S)+-check/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="true" title="true" <?php Tools::checkToDefault($value["value"], "true"); ?>>Włączona opcja</option>
            </select>
            <?php elseif(preg_match("/(\S)+-count/", $value["varname"]) == 1): ?>
            <input class="form-control" type="number" name="<?php echo($value["varname"]); ?>" placeholder="<?php echo($value["varname"]); ?>" value="<?php echo($value["value"]); ?>"/>
            <?php elseif(preg_match("/(\S)+-anim-name/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <?php if(!in_array($value["value"], array_merge(["false"], SuperApp::LoadAnimationNames()))): ?>
                <option value="<?php echo($value["value"]); ?>" title="<?php echo($value["value"]); ?>" <?php Tools::checkToDefault($value["value"], $value["value"]); ?>><?php echo($value["value"]); ?></option>
                <?php endif; ?>
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <?php foreach(SuperApp::LoadAnimationNames() as $anim): ?>
                <option value="<?php echo($anim); ?>" title="<?php echo($anim); ?>" <?php Tools::checkToDefault($value["value"], $anim); ?>>
                    <?php echo((isset(SuperApp::LoadAnimationNamesLanguages()[$anim]) ? SuperApp::LoadAnimationNamesLanguages()[$anim] : $anim)); ?>
                </option>
                <?php endforeach; ?>
            </select>
            <?php elseif((preg_match("/(\S)+-anim-transition/", $value["varname"]) == 1) || (preg_match("/(\S)+-trans-transition/", $value["varname"]) == 1)): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="ease" title="ease" <?php Tools::checkToDefault($value["value"], "ease"); ?>>Wolniej na początku, Szybciej i Wolniej na końcu</option>
                <option value="ease-in" title="ease-in" <?php Tools::checkToDefault($value["value"], "ease-in"); ?>>Wolniej na początku</option>
                <option value="ease-out" title="ease-out" <?php Tools::checkToDefault($value["value"], "ease-out"); ?>>Wolniej na końcu</option>
                <option value="ease-in-out" title="ease-in-out" <?php Tools::checkToDefault($value["value"], "ease-in-out"); ?>>Wolniej na początku i na końcu</option>
                <option value="linear" title="linear" <?php Tools::checkToDefault($value["value"], "linear"); ?>>Przejście Liniowe</option>
                <option value="step-start" title="step-start" <?php Tools::checkToDefault($value["value"], "step-start"); ?>>Początek</option>
                <option value="step-end" title="step-end" <?php Tools::checkToDefault($value["value"], "step-end"); ?>>Koniec</option>
                <option value="initial" title="initial" <?php Tools::checkToDefault($value["value"], "initial"); ?>>Domyślna Wartość</option>
                <option value="inherit" title="inherit" <?php Tools::checkToDefault($value["value"], "inherit"); ?>>Dziedzicz po rodzicu</option>
                <option value="unset" title="unset" <?php Tools::checkToDefault($value["value"], "unset"); ?>>Usuń</option>
            </select>
            <?php elseif(preg_match("/(\S)+-anim-direction/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="normal" title="normal" <?php Tools::checkToDefault($value["value"], "normal"); ?>>Normalnie</option>
                <option value="reverse" title="reverse" <?php Tools::checkToDefault($value["value"], "reverse"); ?>>Odwrócone</option>
                <option value="alternate" title="alternate" <?php Tools::checkToDefault($value["value"], "alternate"); ?>>Zmiana z każdym cyklem, pierwszy normalnie</option>
                <option value="alternate-reverse" title="alternate-reverse" <?php Tools::checkToDefault($value["value"], "alternate-reverse"); ?>>Zmiana z każdym cyklem, pierwszy odwrotnie</option>
                <option value="initial" title="initial" <?php Tools::checkToDefault($value["value"], "initial"); ?>>Domyślna Wartość</option>
                <option value="inherit" title="inherit" <?php Tools::checkToDefault($value["value"], "inherit"); ?>>Dziedzicz po rodzicu</option>
                <option value="unset" title="unset" <?php Tools::checkToDefault($value["value"], "unset"); ?>>Usuń</option>
            </select>
            <?php elseif(preg_match("/(\S)+-anim-mode/", $value["varname"]) == 1): ?>
            <select class="form-control" name="<?php echo($value["varname"]); ?>">
                <option value="false" title="false" <?php Tools::checkToDefault($value["value"], "false"); ?>>Wyłączona opcja</option>
                <option value="forwards" title="forwards" <?php Tools::checkToDefault($value["value"], "forwards"); ?>>Do przodu</option>
                <option value="backwards" title="backwards" <?php Tools::checkToDefault($value["value"], "backwards"); ?>>Do tyłu</option>
                <option value="both" title="both" <?php Tools::checkToDefault($value["value"], "both"); ?>>Oba</option>
                <option value="none" title="none" <?php Tools::checkToDefault($value["value"], "none"); ?>>Usuń</option>
            </select>
            <?php else: ?>
            <input class="form-control" type="text" name="<?php echo($value["varname"]); ?>" placeholder="<?php echo($value["varname"]); ?>" value="<?php echo(htmlentities($value["value"])); ?>"/>
            <?php endif; ?>
            <div class="input-group-append">
                <?php if(preg_match("/(\S)+-bg-img-url/", $value["varname"]) == 1): ?>
                <span class="btn btn-primary input-group-text" data-toggle="modal" data-target="#ImagesRepo" onclick="framerImages('<?php echo(Tools::trimOV($value["varname"])); ?>');">
                    Dodaj zdjęcie
                </span>
                <?php endif; ?>
                <button class="clr-btn btn btn-primary input-group-text">
                    Wyczyść
                </button>
            </div>
        </div></li>
        <?php
    }
    /**
     * Module Manager Renderer
     */
    static function moduleManagerRenderer()
    {
        global $modules_enabled, $lang_mod;
        foreach(SuperApp::LoadAllModules() as $mod)
        {
            ?>
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" id="<?php echo($mod); ?>" name="<?php echo($mod); ?>" placeholder="<?php echo($mod); ?>" <?php Tools::checkToDefaultCheckbox($mod, $modules_enabled); ?>/>
                <label for="<?php echo($mod); ?>" class="form-check-label">
                    <?php echo((isset($lang_mod[$mod]) ? $lang_mod[$mod] : "$mod.scss")); ?>
                </label>
            </div>
            <?php
        }
    }
    /**
     * Token Renderer for specified module
     */
    static function tokenRenderer($mod)
    {
        global $lang_sections;
        $tokens = PW_SCSS__Parser::parse(Tools::modToPath($mod));
        App::saveTokensToJSON($tokens);
        foreach($tokens as $i => $token)
        {
            $v = $token["value"];
            if(Tools::getOptionStatus("debug-test-mode-renderer"))
            {
                var_dump($token);
            }
            if($token["type"] == "section")
            {
                $v = (isset($lang_sections[Tools::trimOV($v)]) ? $lang_sections[Tools::trimOV($v)] : $v);
                if(Tools::getOptionStatus("heading-token-renderer"))
                {
                    echo('<h1 '.Tools::tooltip(Tools::trimOV($token["value"])).'>'.$v.'</h1>');
                }
                else
                {
                    echo("</ul>");
                    echo("</ul>");
                    echo('<ul class="list-group roll">');
                    echo('<li class="list-group-item active" '.Tools::tooltip(Tools::trimOV($token["value"])).'>'."$v".'</li>');
                    echo('<li class="list-group-item"><ul class="list-group roll list-group-base">');
                    echo('<li class="list-group-item active">Baza</li>');
                }
            }
            elseif($token["type"] == "section2")
            {
                $v = (isset($lang_sections[Tools::trimOV($v)]) ? $lang_sections[Tools::trimOV($v)] : $v);
                if(Tools::getOptionStatus("heading-token-renderer"))
                {
                    echo('<h2 '.Tools::tooltip(Tools::trimOV($token["value"])).'>'.$v.'</h2>');
                }
                else
                {
                    echo("</ul>");
                    echo('<ul class="list-group roll">');
                    echo('<li class="list-group-item active" '.Tools::tooltip(Tools::trimOV($token["value"])).'>'."$v".'</li>');
                }
                if(preg_match("/(\S)+-icon-(\S)+/", $tokens[$i+1]["value"]["varname"]) == 1)
                {
                    echo('<li class="list-group-item">');
                    echo('<a class="iconbtn btn btn-success btn-block" title="'.$tokens[$i+1]["value"]["varname"].'" href="ikony?form=..%2Fvariables.php&form-hidden='.$tokens[$i+1]["value"]["varname"].'%26'.$mod.'">Wybierz Ikonę</a>');
                    echo('</li>');
                }
            }
            elseif($token["type"] == "section3")
            {
                $v = (isset($lang_sections[Tools::trimOV($v)]) ? $lang_sections[Tools::trimOV($v)] : $v);
                if(Tools::getOptionStatus("heading-token-renderer"))
                {
                    echo('<h3 '.Tools::tooltip(Tools::trimOV($token["value"])).'>'.$v.'</h3>');
                }
                else
                {
                    echo("</ol>");
                    echo('</li>');
                    echo('<li class="list-group-item">');
                    echo('<ol class="list-group">');
                    echo('<li class="list-group-item active" '.Tools::tooltip(Tools::trimOV($token["value"])).'>'."$v".'</li>');
                }
                if(preg_match("/(\S)+-icon-(\S)+/", $tokens[$i+1]["value"]["varname"]) == 1)
                {
                    echo('<li class="list-group-item">');
                    echo('<a class="iconbtn btn btn-success btn-block" title="'.$tokens[$i+1]["value"]["varname"].'" href="ikony?form=..%2Fvariables.php&form-hidden='.$tokens[$i+1]["value"]["varname"].'%26'.$mod.'">Wybierz Ikonę</a>');
                    echo('</li>');
                }
            }
            elseif($token["type"] == "variable")
            {
                App::inputRenderer($v);
            }
        }
    }
    /**
     * Site Renderer
     * Main Page Renderer
     */
    static function siteRenderer()
    {
        global $modules_enabled, $lang_mod;
        echo('<ul class="nav nav-tabs" id="myTab" role="tablist">');
        foreach($modules_enabled as $mod)
        {
            ?>
            <li class="nav-item">
                <a class="nav-link <?php echo(($mod == "base") ? "active" : ""); ?>" id="<?php echo($mod); ?>-tab" data-toggle="tab" href="#<?php echo($mod); ?>" role="tab" aria-controls="<?php echo($mod); ?>" aria-selected="true">
                    <?php echo((isset($lang_mod[$mod]) ? $lang_mod[$mod] : "$mod.scss")); ?>
                </a>
            </li>
            <?php
        }
        echo('</ul>');
        echo('<div class="tab-content" id="myTabContent">');
        foreach($modules_enabled as $mod)
        {
            echo('<div class="tab-pane fade show '.(($mod == "base") ? "active" : "").'" id="'.$mod.'" role="tabpanel" aria-labelledby="'.$mod.'-tab">');
            App::tokenRenderer($mod);
            echo('</div>');
        }
        echo('</div>');
    }
    /**
     * Theme Changer Renderer
     */
    static function themeSelectorRenderer()
    {
        global $themes, $theme, $lang_theme;
        foreach($themes as $th)
        {
            ?>
            <option value="<?php echo($th); ?>" title="<?php echo($th); ?>" <?php Tools::checkToDefault($theme, $th); ?>>
                <?php echo((isset($lang_theme[$th]) ? $lang_theme[$th] : $th)); ?>
            </option>
            <?php
        }
    }
    /**
     * Icons Process Request Handler
     */
    static function iconsProcessRequest()
    {
        $icon = [];
        $t = $_POST["hidden_value"];
        $t = explode("&", $t);
        $icon["hidden"] = $t[0];
        $icon["mod"] = $t[1];
        $icon["name"] = $_POST["icon_name"];
        $icon["font"] = $_POST["icon_font"];
        $icon["code"] = $_POST["icon_code"];
        $match = [];
        $t = preg_match("/(?P<name>[\S]+)-family/", $icon["hidden"], $match);
        if($t == 1)
        {
            $icon["hidden"] = $match["name"];
        }
        else
        {
            die("Icon string not found!");
        }
        $f = file_get_contents(Tools::modToPath($icon["mod"]));
        $replacing = '/\$'.$icon["hidden"].'-content:[\s]*[\S ]+;/';
        $replace = '$'.$icon["hidden"].'-content: '.$icon["code"].';';
        $f = preg_replace($replacing, $replace, $f);
        $replacing = '/\$'.$icon["hidden"].'-family:[\s]*[\S ]+;/';
        $replace = '$'.$icon["hidden"].'-family: '.$icon["font"].';';
        $f = preg_replace($replacing, $replace, $f);
        file_put_contents(Tools::modToPath($icon["mod"]), $f);
    }
    /**
     * Renderer for API View
     */
    static function apiRenderer()
    {
        global $lang, $warn, $lang_mod, $modules_enabled, $themes, $theme, $lang_theme, $options;
        $api_return = [];
        foreach($modules_enabled as $mod)
        {
            $api_return["modules"][] = [
                "name" => $mod,
                "loc_name" => (isset($lang_mod[$mod]) ? $lang_mod[$mod] : "$mod.scss"),
                "file_name" => "$mod.scss"
            ];
        }
        $api_return["warns"] = $warn;
        $api_return["current_theme"] = $theme;
        foreach($themes as $th)
        {
            $api_return["themes"][] = [
                "name" => $th,
                "loc_name" => (isset($lang_theme[$th]) ? $lang_theme[$th] : $th)
            ];
        }
        $api_return["tokens"] = [];
        foreach($modules_enabled as $mod)
        {
            $api_return["tokens"] = array_merge($api_return["tokens"], App::apiTokenRenderer($mod));
        }
        header('Content-Type: application/json');
        echo(json_encode($api_return));
    }
    /**
     * Renderer of Tokens for API View
     */
    static function apiTokenRenderer($mod)
    {
        $tokens = PW_SCSS__Parser::parse(Tools::modToPath($mod));
        return $tokens;
    }
    /**
     * Module Manager Request Handler
     */
    static function moduleManagerRequest()
    {
        $enabled = array();
        foreach($_POST as $k => $v)
        {
            if($v == "on")
            {
                $enabled[] = $k;
            }
        }
        App::saveEnabledModules($enabled);
    }
    /**
     * Save Enabled Modules
     */
    static function saveEnabledModules($enabled)
    {
        $r = json_decode(
            file_get_contents(
                __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json"
            ),
            true
        );
        $r["enabled"] = array_merge($r["default"], $enabled);
        file_put_contents(
            __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."modules.json",
            json_encode($r, JSON_PRETTY_PRINT)
        );
    }
}
// Process Request
App::processRequest();
?>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Narzędzie do zmiany pliku variables.scss</title>
        <meta name="copyright" value="Patryk Adamczyk © 2018"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <style>
            .code {
                background:white;
                margin: 10px 0px;
                overflow: hidden;
            }
            .code:hover {
                background: #f0f0f0;
            }
            .code:hover>* {
                background: #d0d0d0;
            }
            label
            {
                font-size: 125%;
            }
            ul.list-group
            {
                /*width: 50%;*/
                float: left;
            }
            ul.list-group .active
            {
                text-align: center;
            }
            ul.list-group li:first-child{
                cursor:pointer;
                position:relative;
                display:block !important;
            }
            ul.list-group.roll li{
                /*background-color:grey;*/
               display:none;
            }
            ul.list-group li:first-child:before{}
            .container
            {
                max-width: none;
                display: block;
                align-items: center;
                justify-content: center;
                width: 100%;
            }
            .theme-form-container, .form-container, .theme-add-form-container, .refresher-container
            {
                width: 100%;
                min-width: 90vw;
                margin-left: auto;
                margin-right: auto;
            }
            .theme-form-container form, .form-container form, .theme-add-form-container form
            {
                width: 100%;
            }
            .icons-iframe{
                opacity:0;
                height:0px;
            }
            #IconModal iframe, #ImagesRepo iframe
            {
                min-height: 80vh;
            }
            .tab-pane > ul
            {
                width: 100%;
                max-width: 100%;
                min-width: 100%;
            }
            .refresher-container button, #form__updater_of_themes_form
            {
                width: 33%;
            }
            #form__updater_of_themes
            {
                width: 100%;
            }
            #form__updater_of_themes_form
            {
                margin: 0;
            }
            div.tooltip
            {
                width: 50vw;
            }
            div.tooltip > div.tooltip-inner
            {
                max-width: none;
                width: 50vw;
            }
        </style>
        <script>
            function findInArray(k, arr)
            {
                var ret = 65536;
                $.each(arr, function(index, value){
                    if(value.varname == k)
                    {
                        //console.log(value.value);
                        ret = value.value;
                    }
                });
                return ret;
            }
            function API_Updater()
            {
                $.ajax({
                        url: 'variables.php?API',
                        type: 'GET',
                        success: function(msg) {
                            console.log(msg);
                            var variables = [];
                            $.each(msg.tokens, function( index, value ) {
                                try {
                                    if(value.type == "variable")
                                    {
                                        variables.push(value.value)
                                    }
                                } catch (error) {
                                }
                            });
                            $.each($("input"), function(index, value) {
                                if(typeof findInArray($(value).attr("name"), variables) !== "number")
                                {
                                    $(value).val(findInArray($(value).attr("name"), variables));
                                    //console.log(findInArray($(value).attr("name"), variables)+" : "+$(value).val());
                                }
                            });
                            if($("select[name=theme-variables]").val() !== msg.current_theme)
                            {
                                alert("Ktoś inny zmienił motyw w czasie twojej pracy! Przeładujemy stronę!");
                                window.location.href = "variables.php";
                            }
                            console.log('Odebrano AJAX JSON!');
                        },
                        error: function(msg) {
                            console.error("API_Updater have a problem!!")
                            console.error(msg);
                        }
                    });
            }
            function onBaseRedirect(url)
            {
                url = url.href;
                var patt = new RegExp(/[\S]+\/tools\/variables.php/);
                var res = patt.test(url);
                //console.log(url);
                //console.log(res);
                if(res)
                {
                    //window.location.href = "variables.php";
                    API_Updater();
                    $("#IconModal").modal("hide"); 
                }
            }
            function framerImages($varname)
            {
                var $modal = $("#ImagesRepo");
                $($modal).find("iframe").attr("src", "deps/images.php");
                $($modal).find("iframe").attr("data-varname", $varname);
            }
            function onImagesRed(url)
            {
                url = url.href;
                var patt = new RegExp(/[\S]+\/tools\/deps\/images.php\?[\S]+/);
                var res = patt.test(url);
                //console.log(url);
                //console.log(res);
                if(res)
                {
                    //window.location.href = "variables.php";
                    $.ajax({
                            url: url,
                            type: 'GET',
                            success: function(msg) {
                                console.log(msg);
                                var str = "input[name=" + $("#ImagesRepo").find("iframe").attr("data-varname") + "]";
                                //console.log(str);
                                $(str).val('"' + msg.image_name + '"');
                                $("#ImagesRepo").modal("hide"); 
                            }               
                        });
                }
            }
        </script>
    </head>
    <body>
        <?php App::getSpecialAnnouncement(); ?>
        <?php App::getSaveAnnouncement(); ?>
        <div class="container">
            <?php if(Tools::getOptionStatus("version-header")): ?>
            <div class="row">
                <h4 class="text-center w-100">CMS do Parametryzacji v.<?php echo(PARAMETER_CMS_VERSION); ?></h4>
            </div>
            <?php endif; ?>
            <div class="theme-form-container row">
                <form id="theme-change-form" class="col-12" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="theme-variables-label">Wybierz motyw </span>
                        </div>
                        <select class="form-control" name="theme-variables" id="theme-variables">
                            <?php App::themeSelectorRenderer(); ?>
                        </select>
                    </div>
                </form>
            </div>
            <?php if(isset($options["theme-adding"]) && $options["theme-adding"]): ?>
            <div class="theme-add-form-container row">
                <form id="theme-add-form" class="col-12" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="theme-variables-label">Dodaj nowy motyw </span>
                        </div>
                        <input class="form-control" type="text" name="theme-name" id="theme-name" placeholder="Nazwa Motywu" required/>
                        <div class="input-group-append">
                            <input class="form-control btn btn-primary" type="submit" name="theme-add-submit" value="Dodaj"/>
                        </div>
                    </div>
                </form>
            </div>
            <?php endif; ?>
            <div class="refresher-container row">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModuleManagerModal">
                    Menadżer Modułów
                </button>
                <button class="btn btn-info" onclick="API_Updater();">Odśwież</button>
                <form method="post" id="form__updater_of_themes_form">
                <button type="submit" class="btn btn-primary" name="form__updater_of_themes" id="form__updater_of_themes">
                    Aktualizuj Moduły
                </button>
                </form>
            </div>
            <div class="form-container row">
                <form id="form" method="post">
                <?php
                App::siteRenderer();
                ?>
                <nav class="navbar fixed-bottom navbar-light bg-light">
                    <input form="form" type="hidden" name="dscms_varform_submit" value="ZAPISZ"/>
                    <input form="form" style="font-size: x-large;position: fixed; bottom: 10px; right: 10px;" type="submit" name="dscms_varform_submit" value="ZAPISZ">
                </nav>
                </form>
            </div>
        </div>
        <div class="modal fade" id="IconModal" tabindex="-1" role="dialog" aria-labelledby="IconModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="IconModalLabel">Ikony</h5>
                </div>
                <div class="modal-body">
                    <iframe src="" style="zoom:0.60" width="99.6%" height="250" frameborder="0" onLoad="onBaseRedirect(this.contentWindow.location);"></iframe>
                </div>
                <div class="modal-footer">
                    <!--<a href="variables.php"><button type="button" class="btn btn-primary">Zapisz</button></a>-->
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModuleManagerModal" tabindex="-1" role="dialog" aria-labelledby="ModuleManagerModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModuleManagerModalLabel">Menadżer Modułów</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="manager" method="post">
                        <?php App::moduleManagerRenderer(); ?>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="manager" name="form__module_manager" class="btn btn-primary">Zapisz zmiany</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ImagesRepo" tabindex="-1" role="dialog" aria-labelledby="ImagesRepoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ImagesRepoLabel">Repozytorium Zdjęć</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe src="deps/images.php" style="zoom:0.60" width="99.6%" height="250" frameborder="0" onLoad="onImagesRed(this.contentWindow.location);"></iframe>
                </div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>
        <script>
           $("document").ready(function(){    
               $("ul.list-group li:first-child").click(function(){
                    $(this).parent().toggleClass("roll");
               });
               $("#theme-variables").on("change", function(){
                   $("#theme-change-form").submit();
               });
               $("#form").on( "submit", function( event ) {  
                    event.preventDefault();
                    $.post(
                        'variables.php',
                        $(this).serialize(),
                        function(msg) {
                            var regex = new RegExp(/Fatal\ error/);
                            var test = regex.test(msg)
                            //console.log(test);
                            if(test)
                            {
                                console.log(msg);
                                alert("Błąd!\nBłąd został wysłany do konsoli JavaScript!\n"+msg);
                            }
                            else
                            {
                                //console.log(msg);
                                alert('Zapisano!');
                            }
                        }               
                    );
                });
                $(".iconbtn").on("click" , function(event) {
                
                var $modal = $("#IconModal");
                var frameSrc = $(this).attr("href");
                
                  $($modal).find("iframe").attr("src",frameSrc);
                  $($modal).modal("show"); 

                      return false;
                });

                
                var hiddenInputs = new RegExp(/scss-placeholder-variable[0-9]*/);
                hideInputs(hiddenInputs); 
                 
                function hideInputs(arr){
                    $(".list-group-base").find("input").each(function(){
                        if(arr.test($(this).attr("name"))){
                            <?php if(Tools::getOptionStatus("heading-token-renderer")): ?>
                            $(this).parent().parent().css("display","none");
                            <?php else: ?>
                            $(this).parent().parent().parent().css("display","none");
                            <?php endif; ?>
                        }
                    });
                }
                $(".clr-btn").on("click" , function(event) {
                    $(this).parent().parent().find("input").val("");
                    return false;
                });
                $('[data-toggle="tooltip"]').tooltip({
                    html: true
                });
           });
        </script>
    </body>
</html>
