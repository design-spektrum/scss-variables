<?php
/**
 * SASS Compiler Script API
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright Patryk Adamczyk © 2018
 */
?>
<?php
$base = __DIR__;
require_once("$base/deps/leafo/scssphp/scss.inc.php");
use Leafo\ScssPhp\Compiler;
?>
<?php
/**
 * Class SassCompiler
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * Everything happens right when you run your app, on-the-fly, in pure PHP. No Ruby needed, no configuration needed.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 */
class SassCompiler
{
    /**
     * Compiles all .scss files in a given folder into .css files in a given folder
     *
     * @param string $scss_folder source folder where you have your .scss files
     * @param string $css_folder destination folder where you want your .css files
     * @param string $format_style CSS output format, see http://leafo.net/scssphp/docs/#output_formatting for more.
     */
    static public function run($scss_folder, $css_folder, $format_style = "scss_formatter")
    {
        // get all .scss files from scss folder
        $filelist = glob($scss_folder . "*.scss");
        
        // loop through .scss files and see if any need recompilation
        $has_changes = false;
        foreach ($filelist as $file_path) {
            $css_path = str_replace(array($scss_folder, '.scss'), array($css_folder, '.css'), $file_path);
            if (! realpath($css_path) or filemtime($file_path) > filemtime($css_path)) {
                $has_changes = true;
                break;
            }
        }

        // no files are changed, retun
        if (! $has_changes) return false;

        // scssc will be loaded automatically via Composer
        $scss_compiler = new Compiler();
        // set the path where your _mixins are
        $scss_compiler->setImportPaths($scss_folder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $scss_compiler->setFormatter($format_style);

        // step through all .scss files in that folder
        foreach ($filelist as $file_path) {
            // get scss and css paths
            $scss_path = $file_path;
            $css_path = str_replace(array($scss_folder, '.scss'), array($css_folder, '.css'), $file_path);
            // do not compile if scss has not been recently updated
            if (realpath($css_path) and ! filemtime($scss_path) > filemtime($css_path)) continue;
            // get .scss's content, put it into $string_sass
            $string_sass = file_get_contents($scss_path);
            // compile this SASS code to CSS
            $string_css = $scss_compiler->compile($string_sass);
            // write CSS into file with the same filename, but .css extension
            file_put_contents($css_path, $string_css);
        }
    }
}
/**
 * Version without checking is it was modified
 */
class SassCompiler_NoCheck
{
    static public function fSlashes($code, $characters)
    {
        foreach($characters as $character)
        {
            $code = str_replace("\\$character", $character, $code);
        }
        return $code;
    }
    /**
     * Compiles all .scss files in a given folder into .css files in a given folder
     *
     * @param string $scss_folder source folder where you have your .scss files
     * @param string $css_folder destination folder where you want your .css files
     * @param string $format_style CSS output format, see http://leafo.net/scssphp/docs/#output_formatting for more.
     */
    static public function run($scss_folder, $css_folder, $format_style = "scss_formatter")
    {
        // get all .scss files from scss folder
        $filelist = glob($scss_folder . "*.scss");

        // scssc will be loaded automatically via Composer
        $scss_compiler = new Compiler();
        // set the path where your _mixins are
        $scss_compiler->setImportPaths($scss_folder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $scss_compiler->setFormatter($format_style);

        // step through all .scss files in that folder
        foreach ($filelist as $file_path) {     //echo $file_path.'<br>';
            print("Now Processing: $file_path\n");
            // get scss and css paths
            $scss_path = $file_path;
            $css_path = str_replace(array($scss_folder, '.scss'), array($css_folder, '.css'), $file_path);
            // get .scss's content, put it into $string_sass
            $string_sass = file_get_contents($scss_path);
            // compile this SASS code to CSS
            $string_css = $scss_compiler->compile($string_sass);
            // Escape Characters don't work well so time to try fix it
            $string_css = SassCompiler_NoCheck::fSlashes($string_css, [
                "\\e",
                "\\f",
                "\""
            ]);
            // write CSS into file with the same filename, but .css extension
            file_put_contents($css_path, $string_css);
        }
    }
}
?>
<?php
function LoadThemes()
{
    $r = json_decode(
        file_get_contents(
            __DIR__.DIRECTORY_SEPARATOR."scss-lib".DIRECTORY_SEPARATOR."variables".DIRECTORY_SEPARATOR."themes.json"
        ),
        true
    );
    return array_merge(["default"], $r["themes"]);
}
function SaveToCMSThemes($theme)
{
    $path = __DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."css.config.php";
    $r = include($path);
    $r["theme"] = $theme;
    $r["themes"] = LoadThemes();
    file_put_contents($path, '<?php return '.var_export($r, true).';');
}
if(!isset($_GET["theme"]))
{
    die("Theme not specified!");
}
$th = $_GET["theme"];
$paths = [
    "$base/scss/" => "$base/../public/css/$th/",
    "$base/scss/cms/" => "$base/../public/css/$th/cms/"
];
foreach($paths as $scss => $css)
{
    @mkdir($css);
    SassCompiler_NoCheck::run($scss, $css, "Leafo\ScssPhp\Formatter\Compressed");
}
SaveToCMSThemes($th);
if(isset($_GET["redirect_after"]))
{
    header("Location: ".urldecode($_GET["redirect_after"]));
}
#header('Content-Type: application/json');
#print(json_encode(array_merge(["status" => "success", "paths" => $paths])));
?>